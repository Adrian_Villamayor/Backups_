#!/bin/bash
# -------------------------------------------------------------------------------------------------
# Name:    - Backup-Script
# Version: - 1
# Author:  - Adrián Villamayor

mkdir BBDD_backup
cd BBDD_backup
timeAndDate=`date`

{ # try
cat _bbdd.txt
} || { # catch
echo -e "[$timeAndDate] [BBDD] > No se encuentra el archivo de carga de datos\r\n" >>log.txt
}

#Cogemos el archivo .txt y lo separamos por lineas.
for line in $(cat _bbdd.txt)
#Lo separamos por columnas.
do
	host=$(echo $line | cut -d"|" -f1 | tr -d '[[:space:]]')
	myuser=$(echo $line | cut -d"|" -f2 | tr -d '[[:space:]]') 
	#En este caso eliminamos salto de lineas y espacios.
	mypass=$(echo $line | cut -d"|" -f3 | tr -d '[[:space:]]')
 #Lo guardamos en una array
 declare -A a=(
 	[Host]=$host
 	[User]=$myuser
 	[Pass]=$mypass
 	) 

# Creamos los argumentos para mysql
args="-u"${a[User]}" -p"${a[Pass]}" -h"${a[Host]}" --add-drop-database --add-locks --create-options --complete-insert --comments --disable-keys --dump-date --extended-insert --quick --routines --triggers"                                            

# Creamos la lista en database.list
{ # try
mysql -u${a[User]} -p${a[Pass]} -h${a[Host]} -e 'show databases' | grep -Ev "(Database|information_schema)" > databases.list

} || { # catch
echo -e "[$timeAndDate]  [FTP] > Error de Loggin en ${a[Host]}\r\n" >>log.txt
}

#Volcamos las bbdd, mostrando todas las bbdd y su tamaño
echo "* Se volcarán las siguientes bases de datos:"
{ # try
mysql -u${a[User]} -p${a[Pass]} -h${a[Host]} -e 'select table_schema "Database",convert(sum(data_length+index_length)/1048576, decimal(6,2)) "SIZE (MB)" from information_schema.tables where table_schema!="information_schema" group by table_schema;'

} || { # catch
echo -e "[$timeAndDate]  [FTP] > Error al volcar ${a[User]}\r\n" >>log.txt
}

# Miramos las ddbb y recogemos todos los datos
while read DB
do
	dump="dump_"$DB".sql"
	echo -n $dump"... "
	mysqldump ${args} $DB > $dump
	echo "* OK."
done < databases.list

#Borramos la lista.
rm databases.list
echo "................................................................................................"
done
echo "* Backup finalizado."