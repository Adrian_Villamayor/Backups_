#!/bin/bash
# -------------------------------------------------------------------------------------------------
# Name:    - Backup-Script
# Version: - 1
# Author:  - Adrián Villamayor

#Generamos la carpeta donde se alojaran los Backup.
mkdir "FTP_backup"
cd  "FTP_backup"
timeAndDate=`date`
#Cogemos el archivo .txt y lo separamos por lineas.
{ # try
cat _ftp.txt

} || { # catch
echo -e "[$timeAndDate] [FTP] > No se encuentra el archivo de carga de datos\r\n" >>log.txt
}

for line in $(cat _ftp.txt) 
#Lo separamos por columnas.
do
    host=$(echo $line | cut -d"|" -f1 | tr -d '[[:space:]]')
    myuser=$(echo $line | cut -d"|" -f2 | tr -d '[[:space:]]')
    #En este caso eliminamos salto de lineas y espacios.
    mypass=$(echo $line | cut -d"|" -f3 | tr -d '[[:space:]]')     
    #Lo guardamos en una array
    declare -A a=(
        [Host]=$host
        [User]=$myuser
        [Pass]=$mypass
        ) 

#Obtenemos todos los archivos del servidor FTP
{ # try
wget -r ftp://${a[User]}:${a[Pass]}@${a[Host]} 

} || { # catch
echo -e "[$timeAndDate]  [FTP] > Error de Loggin en ${a[Host]}\r\n" >>log.txt
}

#Comprimimos la carpeta recien generada
echo "* Comprimiendo"
{ # try
sudo zip -r ${a[Host]}.zip ${a[Host]}

} || { # catch
echo -e "[$timeAndDate]  [FTP] > Error al comprimir los archivos de ${a[Host]}\r\n" >>log.txt
}

#Eliminamos los elementos no comprimidos 
rm -r ${a[Host]}
echo "................................................................................................"
done
echo "* Backup finalizado." 